/*
   Nama Program : rata2.cc
   Tgl buat     : 7 November 2023
   Deskripsi    : menghitung rata-rata
*/

#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

    int i = 0, N = 0;  // counter and number of data
    float Data = 0.0, Rata = 0.0, Total = 0.0;

    cout << "Banyaknya data : ";
    cin >> N;
    Total = 0;

    for (i = 1; i <= N; i++) {
        cout << "Data ke " << i << " : ";
        cin >> Data;
        Total += Data;
    }

    Rata = Total / N;
    cout << "Banyaknya Data : " << N << endl;
    cout << "Total Nilai Data : " <<  setprecision(2) << Total << endl;
    cout << "Rata-rata nilai Data : " << setprecision(2) << Rata << endl;

  return 0;
}



