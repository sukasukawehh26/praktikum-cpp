#include <iostream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

    int N = 0;  // counter and number of data
    float Data = 0.0, Rata = 0.0, Total = 0.0;

    Total = 0;

    do {
        cout << "Data ke " << N+1 << " : ";
        cin >> Data;
        if(Data!=0)
        { 
         Total += Data;
         N++;
        } 
        
    }while(Data!=0);

    Rata = Total / N;
    cout << "Banyaknya Data : " << N << endl;
    cout << "Total Nilai Data : " <<  setprecision(2) << Total << endl;
    cout << "Rata-rata nilai Data : " << setprecision(2) << Rata << endl;

  return 0;
}

/*

Data ke 1 : 1
Data ke 2 : 2
Data ke 3 : 3
Data ke 4 : 4
Data ke 5 : 5
Data ke 6 : 0
Banyaknya Data : 5
Total Nilai Data : 15
Rata-rata nilai Data : 3

*/
