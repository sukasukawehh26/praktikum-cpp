// Online C++ compiler to run C++ program online
#include <iostream>

using namespace std;

void input(int A[3][3],int ni,int nj)
{
    cout<<"Mengisi matrik A :"<<endl;
    for(int i=0;i<ni;i++)
    {
        for(int j=0;j<nj;j++)
        {
            cout<<"A["<<i+1<<","<<j+1<<"] = ";
            cin>>A[i][j];
        }
    }
}

void output(int A[3][3],int ni,int nj)
{
    cout<<"Mencetak matrik A :"<<endl;
    for(int i=0;i<ni;i++)
    {
        for(int j=0;j<nj;j++)
        {
            cout<<A[i][j]<<" ";
        }
        cout<<endl;
    }
}

int main() {
    // Write C++ code here
    int A[3][3];
    input(A,3,3);
    output(A,3,3);
    return 0;
}
