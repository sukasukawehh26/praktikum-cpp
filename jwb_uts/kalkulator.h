#include <iostream>
#include <stdlib.h>

using namespace std;

void kalkulator()
{
    system("clear");

    float bil1 =0.0;
    float bil2 = 0.0;
    char  opt  =' ';
    float hsl  = 0.0;

    cout << "*****************************" << endl;
    cout << "*    Kalkulator Sederhana   *" << endl;
    cout << "*****************************" << endl;
    cout << endl;
    cout << "Masukkan Bilangan Ke 1     : ";cin >> bil1;
    cout << "Masukan Operator (x,:,+,-) : ";cin >> opt;
    cout << "Masukkan Bilangan Ke 2     : ";cin >> bil2;
    
    switch (opt)
    {
    case 'x':
        hsl = bil1 * bil2;
        break;
    case ':':
        hsl = bil1 / bil2;
        break;
    case '+':
        hsl = bil1 + bil2;
        break;
    case '-':
        hsl = bil1 - bil2;
        break;        
    
    }

    cout << "Bilangan ke 1 " << opt << " Bilangan ke 2 = " 
         << bil1 << " " << opt << " " << bil2 << " = " << hsl << endl;
    
    cout << endl;
    cout << "Kembali ke menu dalam 5 detik !!!" << endl;
}