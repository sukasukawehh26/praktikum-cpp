#include <iostream>
#include <stdlib.h>
#include <iomanip>

using namespace std;

float Celcius_Fahrenheit(float c)
{
    float hsl=0.0;
    hsl = (c * (9/5))+32;
    return hsl;
}

float Celcius_Kelvin(float c)
{
    float hsl=0.0;
    hsl = c + 273.15;
    return hsl;
}

void konversi()
{
    system("clear");
    
    int awal = 0.0;
    int akhir = 0.0;
    int step = 1 ;
    float c = 0.0;
    float f = 0.0;
    float k = 0.0;

    cout << "*****************************" << endl;
    cout << "*       Tabel Konversi      *" << endl;
    cout << "*****************************" << endl;
    cout << endl;

    cout << "Masukan suhu awal  = ";cin >> awal;
    cout << "Masukan step       = ";cin >> step;
    cout << "Masukan suhu akhir = ";cin >> akhir;
    cout << endl;

    cout << "---------------------------------" << endl; 
    cout << "| Celcius | Fahrenheit | Kelvin |" << endl; 
    cout << "---------------------------------" << endl; 
    
    for (int i = awal; i < akhir; i+=step)
    {
     c = i;
     f = Celcius_Fahrenheit(c);
     k = Celcius_Kelvin(c);
     cout << "|" << setw(5) << c <<setw(5) << "|" << setw(7) << f << setw(6) << "|" << setw(7) << k << setw(2) << "|" << endl; 
    }  

    cout << "---------------------------------" << endl; 

    
    cout << endl; 
    cout << "Kembali ke menu dalam 5 detik !!!" << endl;
}