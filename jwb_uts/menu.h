#include <iostream>
#include <stdlib.h>
#include <unistd.h>

#include "kalkulator.h"
#include "konversi.h"
#include "bintang.h"

using namespace std;

int menu()
{
    int pilih=0;
    
    system("clear");

    cout << "*****************************" << endl;
    cout << "*        Jawaban UTS        *" << endl;
    cout << "*****************************" << endl;
    cout << endl;
    cout << "*****************************" << endl;
    cout << "*            Menu           *" << endl;
    cout << "*****************************" << endl;
    cout << "* 1. Kalkulator Sederhana   *" << endl;
    cout << "* 2. Tabel Konversi Suhu    *" << endl;
    cout << "* 3. Cetak Bintang          *" << endl;
    cout << "*****************************" << endl;
    cout << "* 0. Keluar                 *" << endl;
    cout << "*****************************" << endl;
    cout << "Pilih Menu : ";cin >> pilih;
    return pilih;
}

void menu_dipilih(int no_menu)
{
    switch (no_menu)
    {
    case 1:
        kalkulator();
        sleep(5);
        break;
    case 2:
        konversi();
        sleep(5);
        break;    
    case 3:
        bintang();
        sleep(5);
        break;
    case 0:
        cout << "Keluar Program dalam 2 detik !!!" << endl;
        sleep(2);
        break;
    default:
        cout << "Salah Masukan Menu !!!" << endl;
        sleep(2);
        break;
    }
}