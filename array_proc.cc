// Online C++ compiler to run C++ program online
#include <iostream>
using namespace std;

void input_nilai(int nilai[],int n)
{
   cout<<"Mengisi elemen array : "<<endl;
   for(int i=0;i<n;i++)
   {
     cout<<"Nilai ke-"<<i+1<<" = ";cin>>nilai[i];  
   }
}

void cetak_nilai(int nilai[],int n)
{
   cout<<"Mencetak elemen array : "<<endl;
   for(int i=0;i<n;i++)
   {
      cout<<"Nilai ke-"<<i+1<<" = "<<nilai[i]<<endl; 
   }
}

int main() {
    // Write C++ code here
    int nilai[3]={0,0,0};
    input_nilai(nilai,3);
    cetak_nilai(nilai,3);
    return 0;
}
