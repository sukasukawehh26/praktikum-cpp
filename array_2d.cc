// Online C++ compiler to run C++ program online
#include <iostream>

using namespace std;

int main() {
    // Write C++ code here
    int A[3][3];
    
    cout<<"Mengisi matrik A :"<<endl;
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            cout<<"A["<<i+1<<","<<j+1<<"] = ";
            cin>>A[i][j];
        }
    }

    cout<<"Mencetak matrik A :"<<endl;
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            cout<<A[i][j]<<" ";
        }
        cout<<endl;
    }
    
    return 0;
}
